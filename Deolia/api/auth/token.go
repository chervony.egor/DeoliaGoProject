package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"time"
)

var secretkye string = "SuperKey"

func CreateToken(id uint64) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["id"] = id
	claims["issue_date"] = time.Now()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secretkye))
}

func ParseToken(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretkye), nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, errors.New("Invalid token or claims")
	}

	return claims, nil
}

func TokenGetId(tokenString string) uint64 {
	claims, _ := ParseToken(tokenString)
	return uint64(claims["id"].(float64))
}
