package models

import (
	"gorm.io/gorm"
	"runtime"
)

type Inventory struct {
	ID       uint64
	UserID   uint64
	ItemID   uint64
	Quantity uint64
}

type InventoryInfo struct {
	ID       uint64
	Name     string
	Rarity   string
	Effect   uint64
	Quantity uint64
}

func (inventory *Inventory) AddItem(db *gorm.DB, uid uint64, iId uint64) {
	findedInventory := Inventory{}
	err := db.Debug().Model(&Inventory{}).Where("user_id = ? AND item_id = ?", uid, iId).First(&findedInventory).Error

	if err == nil {
		findedInventory.Quantity++
		db.Debug().Save(&findedInventory)
	} else {
		db.Debug().Create(&inventory)
	}
}

func (inventory *Inventory) GetUserInventory(db *gorm.DB, uid uint64) (*[]InventoryInfo, error) {
	findedUserInventories := []Inventory{}
	findedItems := []Item{}
	ids := []uint64{}
	resultItems := []InventoryInfo{}
	result := db.Debug().Model(&Inventory{}).Where("user_id = ? AND quantity > 0", uid).Find(&findedUserInventories)
	if result.Error != nil {
		return nil, result.Error
	}
	for _, element := range findedUserInventories {
		ids = append(ids, element.ItemID)
	}
	result = db.Debug().Model(&Item{}).Where("id IN ?", ids).Find(&findedItems)
	if result.Error != nil {
		return nil, result.Error
	}
	for index, element := range findedItems {
		resultItems = append(resultItems, InventoryInfo{
			ID:       element.ID,
			Name:     element.Name,
			Rarity:   element.Rarity,
			Effect:   element.Effect,
			Quantity: findedUserInventories[index].Quantity,
		})
	}

	runtime.GC()
	return &resultItems, nil

}
