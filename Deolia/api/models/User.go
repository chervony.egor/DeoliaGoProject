package models

import (
	"gorm.io/gorm"
	_ "gorm.io/gorm/migrator"
	"html"
	"strings"
)

type User struct {
	ID         uint64 `gorm:"primary_key; auto_increment" json:"id"`
	Login      string `gorm:"size:255;not null;unique" json:"login"`
	Email      string `gorm:"size:100;not null;unique" json:"email"`
	Password   string `gorm:"size:100;not null;" json:"password"`
	Experience uint64 `json:"experience"`
	Money      uint64 `json:"money"`
	Inventorys []Inventory
	Shops      []Shop
}

func (u User) PrepareUser() {
	u.Login = html.EscapeString(strings.TrimSpace(u.Login))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.Password = html.EscapeString(strings.TrimSpace(u.Password))
	if u.Experience == 0 {
		u.Experience = 0
	}
	if u.Money == 0 {
		u.Money = 0
	}

}

func (u *User) SaveUser(db *gorm.DB) uint64 {
	db.Debug().Create(&u)
	return u.ID
}

func (u *User) FindAllUsers(db *gorm.DB) (*[]User, error) {
	users := []User{}

	result := db.Debug().Model(&User{}).Preload("Inventorys").Preload("Shops").Find(&users)
	if result.Error != nil {
		return nil, result.Error
	}
	return &users, nil
}

func (u *User) FindUserByIdD(db *gorm.DB, uid uint64) (*User, error) {
	result := db.Debug().Model(User{}).Where("id = ?", uid).Preload("Inventorys").Preload("Shops").Take(&u)
	if result.Error != nil {
		return nil, result.Error
	}
	return u, nil
}

func (u *User) UpdateUserLogin(db *gorm.DB, uid uint64) *User {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"login": u.Login,
		},
	)
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u)
	return u
}

func (u *User) UpdateUserEmail(db *gorm.DB, uid uint64) *User {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"email": u.Email,
		},
	)
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u)
	return u
}

func (u *User) UpdateUserPassword(db *gorm.DB, uid uint64) *User {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"password": u.Password,
		},
	)
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u)
	return u
}

func (u *User) UpdateUserExperience(db *gorm.DB, uid uint64) *User {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"experience": u.Experience,
		},
	)
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u)
	return u
}
func (u *User) UpdateUserMoney(db *gorm.DB, uid uint64) *User {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"money": u.Money,
		},
	)
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u)
	return u
}

func (u *User) DeleteUser(db *gorm.DB, uid uint64) int64 {
	db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).Delete(&User{})
	return db.RowsAffected
}
