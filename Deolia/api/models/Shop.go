package models

import (
	"errors"
	"gorm.io/gorm"
	"runtime"
)

type Shop struct {
	ID       uint64
	UserID   uint64
	ItemID   uint64
	Quantity uint64
	Price    uint64
}

type ShopInfo struct {
	UserName   string
	ItemID     uint64
	ItemName   string
	ItemRarity string
	ItemEffect uint64
	Quantity   uint64
	Price      uint64
}

func (shop *Shop) GetShop(db *gorm.DB, uid uint64) (*[]ShopInfo, error) {

	shopContent := []Shop{}
	shopInfo := []ShopInfo{}
	result := db.Debug().Model(&Shop{}).Where("user_id <> ? AND quantity > 0", uid).Find(&shopContent)
	if result.Error != nil {
		return nil, result.Error
	}
	for _, element := range shopContent {
		user := &User{}
		item := &Item{}
		db.Debug().Model(&User{}).Where("id = ?", element.UserID).First(&user)
		db.Debug().Model(&Item{}).Where("id = ?", element.ItemID).First(&item)
		shopInfo = append(shopInfo, ShopInfo{
			UserName:   user.Login,
			ItemID:     item.ID,
			ItemName:   item.Name,
			ItemRarity: item.Rarity,
			ItemEffect: item.Effect,
			Quantity:   element.Quantity,
			Price:      element.Price,
		})
	}
	runtime.GC()
	return &shopInfo, nil
}

func (shop *Shop) GetSellingItem(db *gorm.DB, uid uint64) (*[]ShopInfo, error) {
	shopContent := []Shop{}
	shopInfo := []ShopInfo{}
	result := db.Debug().Model(&Shop{}).Where("user_id = ?", uid).Find(&shopContent)
	if result.Error != nil {
		return nil, result.Error
	}
	for _, element := range shopContent {
		user := &User{}
		item := &Item{}
		db.Debug().Model(&User{}).Where("id = ?", element.UserID).First(&user)
		db.Debug().Model(&Item{}).Where("id = ?", element.ItemID).First(&item)
		shopInfo = append(shopInfo, ShopInfo{
			UserName:   user.Login,
			ItemName:   item.Name,
			ItemRarity: item.Rarity,
			ItemEffect: item.Effect,
			Quantity:   element.Quantity,
			Price:      element.Price,
		})
	}
	runtime.GC()
	return &shopInfo, nil
}

func (shop *Shop) CreateOrder(db *gorm.DB, uid uint64, iId uint64, price uint64) (uint64, error) {
	findedUser := &User{}
	err := db.Debug().Model(&User{}).Where("id = ?", uid).First(&findedUser).Error
	if err != nil {
		return 0, err
	}

	inventoryItem := &Inventory{}
	err = db.Debug().Model(&Inventory{}).Where("user_id = ? AND item_id = ? AND quantity > 0", findedUser.ID, iId).First(&inventoryItem).Error
	if err != nil {
		return 0, err
	}

	shopItem := &Shop{}
	err = db.Debug().Model(&Shop{}).Where("user_id = ? AND item_id = ? AND price = ?", findedUser.ID, inventoryItem.ItemID, price).First(&shopItem).Error

	if err == nil {
		shopItem.Quantity++
		db.Debug().Save(&shopItem)
		inventoryItem.Quantity--
		db.Debug().Save(&inventoryItem)
		return shopItem.ID, nil
	} else {
		inventoryItem.Quantity--
		db.Debug().Save(&inventoryItem)
		db.Debug().Create(&shop)
		return shop.ID, nil
	}
}

func (shop *Shop) DeleteShopItem(db *gorm.DB, uid uint64, iId uint64) (uint64, error) {
	shopItem := Shop{}
	err := db.Debug().Model(&Shop{}).Where("user_id = ? AND item_id = ? AND quantity > 0", uid, iId).First(&shopItem).Error
	if err != nil {
		return 0, err
	}

	inventoryItem := &Inventory{}
	err = db.Debug().Model(&Inventory{}).Where("user_id = ? AND item_id = ?", uid, iId).First(&inventoryItem).Error
	if err != nil {
		return 0, err
	}

	shopItem.Quantity--
	db.Debug().Save(&shopItem)
	inventoryItem.Quantity++
	db.Debug().Save(&inventoryItem)
	return shopItem.ID, nil
}

func (shop *Shop) BuyItem(db *gorm.DB, uid uint64, iId uint64) (uint64, error) {

	buyerUser := User{}
	err := db.Debug().Model(&User{}).Where("id = ?", uid).First(&buyerUser).Error
	if err != nil {
		return 0, errors.New("cant find buyer")
	}

	shopItem := Shop{}
	err = db.Debug().Model(&Shop{}).Where("item_id = ? AND quantity > 0", iId).First(&shopItem).Error
	if err != nil {
		return 0, errors.New("cant find item")
	}

	inventoryItem := Inventory{
		UserID:   buyerUser.ID,
		ItemID:   shopItem.ItemID,
		Quantity: 1,
	}
	/*err = db.Debug().Model(&Inventory{}).Where("user_id = ? AND item_id = ?", buyerUser.ID, shopItem.ItemID).First(&inventoryItem).Error
	if err != nil {
		return 0, err
	}*/

	sellerUser := User{}

	err = db.Debug().Model(&User{}).Where("id = ?", shopItem.UserID).First(&sellerUser).Error
	if err != nil {
		return 0, errors.New("Cant find seller")
	}

	if buyerUser.Money < shopItem.Price {
		return 0, errors.New("Not enough money")
	}
	if shopItem.Quantity == 0 {
		return 0, errors.New("No items left")
	}
	buyerUser.Money -= shopItem.Price
	db.Debug().Save(&buyerUser)
	sellerUser.Money += shopItem.Price
	db.Debug().Save(&sellerUser)
	shopItem.Quantity--
	db.Debug().Save(&shopItem)
	inventoryItem.AddItem(db, buyerUser.ID, shopItem.ItemID)
	return shopItem.ItemID, nil
}
