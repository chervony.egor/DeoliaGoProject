package models

import (
	"gorm.io/gorm"
	"math/rand"
	"time"
)

type Item struct {
	ID         uint64 `gorm:"primary_key; auto_increment" json:"id"`
	Name       string `gorm:"not null;" json:"name"`
	Rarity     string `gorm:"not null;" json:"rarity"`
	Effect     uint64 `gorm:"not null;" json:"effect"`
	Quantity   uint64 `json:"quantity,omitempty"`
	Inventorys []Inventory
	Shops      []Shop
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func GenerateItemName() string {
	RarityList := []string{
		"Axe", "Sword", "Spear", "Bow", "Dagger",
		"Helmet", "Chestplate", "Leggings"}
	return RarityList[rand.Intn(len(RarityList))]
}

func GenerateRarity() string {
	RarityList := []string{"Standard", "Rare", "Legendary", "Divine"}
	percent := rand.Intn(101)

	if percent >= 0 && percent <= 60 {
		return RarityList[0]
	} else if percent <= 90 {
		return RarityList[1]
	} else if percent <= 98 {
		return RarityList[2]
	} else if percent <= 100 {
		return RarityList[3]
	}

	return RarityList[0]
}

func GenerateNumberByRarity(rarity string) uint64 {
	switch rarity {
	case "Standard":
		return uint64(rand.Intn(35) + 1)
	case "Rare":
		return uint64(rand.Intn(115) + 35)
	case "Legendary":
		return uint64(rand.Intn(150) + 150)
	case "Divine":
		return uint64(rand.Intn(200) + 300)
	default:
		return uint64(rand.Intn(35) + 1)
	}
}

func (item *Item) GenerateItem() *Item {
	item.Name = GenerateItemName()
	item.Rarity = GenerateRarity()
	item.Effect = GenerateNumberByRarity(item.Rarity)
	item.Quantity = 1
	return item
}

func (item *Item) SaveItem(db *gorm.DB) {
	findedItem := Item{}
	err := db.Debug().Model(&Item{}).Where("name = ? AND rarity = ? AND effect = ?", item.Name, item.Rarity, item.Effect).First(&findedItem).Error

	if err == nil {
		findedItem.Quantity++
		db.Debug().Save(&findedItem)
	} else {
		item.Quantity = 1
		db.Debug().Create(&item)
	}
}
