package controllers

import (
	"Deolia/api/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	_ "gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Server struct {
	DB     *gorm.DB
	Router *gin.Engine
}

func (server *Server) Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {
	var err error
	if Dbdriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		server.DB, err = gorm.Open(postgres.Open(DBURL))
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", Dbdriver)
			return
		} else {
			fmt.Printf("We are connected to the %s database\n", Dbdriver)
		}
	} else {
		fmt.Println("Unknown Driver")
		return
	}

	server.DB.Debug().AutoMigrate(
		&models.User{},
		&models.Item{},
		&models.Inventory{},
		&models.Shop{},
	)
	server.Router = gin.Default()
	server.Router.Static("/static", "../../static")

	server.Router.Use(corsMiddleware())

	server.initializeRoutes()

	/*server.Router.Run(":8080")*/

}

func (server *Server) Run() {
	server.Router.Run(":8080")

}

func corsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:8081")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

/*func (server Server) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, server.Router))
}*/
