package controllers

import (
	"Deolia/api/auth"
	"Deolia/api/models"
	"Deolia/api/security"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (server *Server) CreateUser(context *gin.Context) {
	body, _ := ioutil.ReadAll(context.Request.Body)
	newUser := models.User{}
	json.Unmarshal(body, &newUser)
	newUser.PrepareUser()

	existingUser := models.User{}

	server.DB.Model(&models.User{}).Where("login = ?", newUser.Login).First(&existingUser)
	if existingUser.Login == newUser.Login {
		context.JSON(http.StatusConflict, "Login already exists")
		return
	}

	server.DB.Model(&models.User{}).Where("email = ?", newUser.Email).First(&existingUser)
	if existingUser.Email == newUser.Email {
		context.JSON(http.StatusConflict, "Email already exists")
		return
	}
	hashedPass, err := security.Hash(newUser.Password)
	if err != nil {
		context.JSON(http.StatusConflict, err)
	}
	hashedPassString := string(hashedPass)
	newUser.Password = hashedPassString
	newUser.SaveUser(server.DB)
	context.JSON(http.StatusCreated, newUser)
}

func (server *Server) GetAllUsers(context *gin.Context) {
	user := models.User{}
	users, err := user.FindAllUsers(server.DB)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error ": err})
		return
	}
	context.JSON(http.StatusOK, users)
}

func (server *Server) GetUserByID(context *gin.Context) {
	/*contextUserID := context.Param("id")*/
	//context.Header("")
	//var err error
	//body, _ := ioutil.ReadAll(context.Request.Body)
	//requestBody := map[string]string{}
	//err = json.Unmarshal(body, &requestBody)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Cannot unmarshal body ")
	//	return
	//}
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}

	authHeader := context.Request.Header.Get("Authorization")
	fmt.Println(authHeader)
	userID := auth.TokenGetId(authHeader)

	user := models.User{}
	gottenUser, err := user.FindUserByIdD(server.DB, userID)
	if err != nil {
		context.JSON(http.StatusNotFound, "User not found")
		return
	}
	context.JSON(http.StatusOK, gottenUser)
}

func (server *Server) UpdateUserLogin(context *gin.Context) {
	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request")
		return
	}

	findedUser := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&findedUser).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	existingUser := models.User{}
	server.DB.Debug().Model(&models.User{}).Where("login = ?", requestBody["login"]).First(&existingUser)
	if existingUser.Login == requestBody["login"] {
		context.JSON(http.StatusConflict, "Login is already busy")
		return
	}

	newUser := models.User{}

	newUser = findedUser
	newUser.Login = requestBody["login"]
	newUser.PrepareUser()
	updatedUser := newUser.UpdateUserLogin(server.DB, userID)
	context.JSON(http.StatusOK, updatedUser)
}

func (server *Server) UpdateUserEmail(context *gin.Context) {
	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request")
		return
	}

	findedUser := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&findedUser).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	existingUser := models.User{}
	server.DB.Debug().Model(&models.User{}).Where("email = ?", requestBody["email"]).First(&existingUser)
	if existingUser.Login == requestBody["email"] {
		context.JSON(http.StatusConflict, "Email is already busy")
		return
	}

	newUser := models.User{}

	newUser = findedUser
	newUser.Email = requestBody["email"]
	newUser.PrepareUser()
	updatedUser := newUser.UpdateUserEmail(server.DB, userID)
	context.JSON(http.StatusOK, updatedUser)
}

func (server *Server) UpdateUserPassword(context *gin.Context) {
	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request")
		return
	}

	findedUser := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&findedUser).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	newUser := models.User{}

	newUser = findedUser
	newUser.Password = requestBody["password"]
	hashedPass, err := security.Hash(newUser.Password)
	if err != nil {
		context.JSON(http.StatusConflict, err)
	}
	hashedPassString := string(hashedPass)
	newUser.Password = hashedPassString
	newUser.PrepareUser()
	updatedUser := newUser.UpdateUserPassword(server.DB, userID)
	context.JSON(http.StatusOK, updatedUser)
}

func (server *Server) UpdateUserExperience(context *gin.Context) {
	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request(UserID)")
		return
	}
	experience, err := strconv.ParseUint(requestBody["experience"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request(Experience)")
		return
	}

	findedUser := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&findedUser).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	newUser := models.User{}

	newUser = findedUser
	newUser.Experience = experience
	newUser.PrepareUser()
	updatedUser := newUser.UpdateUserExperience(server.DB, userID)
	context.JSON(http.StatusOK, updatedUser)
}

func (server *Server) UpdateUserMoney(context *gin.Context) {
	/*	contextUserID := context.Param("id")
		contextExperience := context.Param("money")*/

	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request(UserID)")
		return
	}
	money, err := strconv.ParseUint(requestBody["money"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request(Money)")
		return
	}

	findedUser := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&findedUser).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	newUser := models.User{}

	newUser = findedUser
	newUser.Money = money
	newUser.PrepareUser()
	updatedUser := newUser.UpdateUserMoney(server.DB, userID)
	context.JSON(http.StatusOK, updatedUser)
}

func (server *Server) DeleteUser(context *gin.Context) {
	/*contextUserID := context.Param("id")*/
	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusCreated, "Invalid request(UserID)")
		return
	}

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("id = ?", userID).Take(&user).Error
	if err != nil {
		context.JSON(http.StatusConflict, "User not found")
		return
	}

	user.DeleteUser(server.DB, userID)
	context.JSON(http.StatusOK, fmt.Sprintf("User %d deleted", userID))
}
