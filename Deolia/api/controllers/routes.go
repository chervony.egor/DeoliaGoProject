package controllers

import "github.com/gin-gonic/gin"

func (server *Server) initializeRoutes() {
	base := server.Router.Group("/web/")
	{
		base.GET("/", func(context *gin.Context) {
			context.File("./static/index.html")
		})
	}

	v1 := server.Router.Group("/api/")
	{
		//User

		//POST
		v1.POST("/createUser", server.CreateUser)
		v1.POST("/step", server.Step)

		//GET
		v1.GET("/getUsers", server.GetAllUsers)
		v1.GET("/getUser", server.GetUserByID)

		//PUT
		v1.PUT("/updateUserLogin", server.UpdateUserLogin)
		v1.PUT("/updateUserEmail", server.UpdateUserEmail)
		v1.PUT("/updateUserPassword", server.UpdateUserPassword)
		v1.PUT("/updateUserExperience", server.UpdateUserExperience)
		v1.PUT("/updateUserMoney", server.UpdateUserMoney)

		//DELETE
		v1.DELETE("/deleteUser", server.DeleteUser)

		//Item

		//GET
		/*v1.POST("/item", server.GenerateItem)*/

		//Shop

		//POST
		v1.POST("/sell", server.CreateOrder)
		v1.POST("/buy", server.BuyItem)

		//GET
		v1.GET("/getShop", server.GetShop)

		//PUT
		v1.PUT("/getSellingItem", server.GetShopUserItem)

		//Inventory

		//PUT
		v1.PUT("/getInventory", server.GetUserInventories)

		//Login
		//POST
		v1.POST("/login", server.Login)
		v1.POST("/parse", server.Parse)
	}
}
