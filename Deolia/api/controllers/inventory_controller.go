package controllers

import (
	"Deolia/api/auth"
	"Deolia/api/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (server *Server) GetUserInventories(context *gin.Context) {
	//body, _ := ioutil.ReadAll(context.Request.Body)
	//requestBody := map[string]string{}
	//err := json.Unmarshal(body, &requestBody)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Cannot unmarshal body ")
	//	return
	//}
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}
	var err error

	authHeader := context.Request.Header.Get("Authorization")
	userID := auth.TokenGetId(authHeader)

	inventory := models.Inventory{}

	result, err := inventory.GetUserInventory(server.DB, userID)
	if err != nil {
		context.JSON(http.StatusConflict, "Conflict")
		return
	}
	context.JSON(http.StatusOK, result)
}
