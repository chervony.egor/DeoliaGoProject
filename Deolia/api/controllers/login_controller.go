package controllers

import (
	"Deolia/api/auth"
	"Deolia/api/models"
	"Deolia/api/security"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"net/http"
)

func (server *Server) Login(context *gin.Context) {

	var err error
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}
	userLogin := requestBody["login"]
	if err != nil {
		context.JSON(http.StatusConflict, "Invalid request")
		return
	}
	userPassword := requestBody["password"]
	if err != nil {
		context.JSON(http.StatusConflict, "Invalid request")
		return
	}

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("login = ?", userLogin).Take(&user).Error
	if err != nil {
		fmt.Println("this is the error getting the user: ", err)
		context.JSON(http.StatusConflict, err)
		return
	}

	err = security.VerifyPassword(user.Password, userPassword)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		fmt.Println("this is the error hashing the password: ", err)
		context.JSON(http.StatusConflict, err)
		return
	}

	token, err := auth.CreateToken(user.ID)
	if err != nil {
		fmt.Println("this is the error creating the token: ", err)
		context.JSON(http.StatusConflict, err)
		return
	}

	context.JSON(http.StatusOK, token)
}

func (server *Server) Parse(context *gin.Context) {
	var err error
	body, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		context.JSON(http.StatusConflict, "Failed to read request body")
		return
	}

	requestBody := map[string]string{}
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body")
		return
	}

	token, ok := requestBody["token"]
	if !ok {
		context.JSON(http.StatusConflict, "Token missing in the request")
		return
	}

	claims, err := auth.ParseToken(token)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "Invalid token")
		return
	}

	context.JSON(http.StatusOK, claims)
}
