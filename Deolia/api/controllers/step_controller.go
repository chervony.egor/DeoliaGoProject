package controllers

import (
	"Deolia/api/auth"
	"Deolia/api/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"math/rand"
	"net/http"
	"time"
)

func (server *Server) Step(context *gin.Context) {
	rand.Seed(time.Now().UnixNano())

	var err error

	authHeader := context.Request.Header.Get("Authorization")
	userID := auth.TokenGetId(authHeader)

	//body, _ := ioutil.ReadAll(context.Request.Body)
	//requestBody := map[string]string{}
	//err = json.Unmarshal(body, &requestBody)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Cannot unmarshal body ")
	//	return
	//}
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}
	user := models.User{}
	gottenUser, err := user.FindUserByIdD(server.DB, userID)
	if err != nil {
		context.JSON(http.StatusNotFound, "User not found")
		return
	}

	randomNumber := rand.Float64()
	if randomNumber <= 0.32 {
		//nothing
		context.JSON(http.StatusOK, "You found nothing \nNever give up! Try again ")
	} else if randomNumber <= 0.64 {
		//exp
		updatedUser := gottenUser
		profit := uint64(1 + float64(updatedUser.Experience)*(0.01+rand.Float64()*(0.05-0.01)))
		fmt.Println(profit)
		updatedUser.Experience += profit
		updatedUser.UpdateUserExperience(server.DB, userID)
		context.JSON(200, fmt.Sprintf("You got %d point(s)", profit))
	} else if randomNumber <= 0.96 {
		//money
		updatedUser := gottenUser
		profit := 1 + uint64(float64(updatedUser.Experience)*(0.01*rand.Float64()*(0.04-0.01))+float64(updatedUser.Experience)*(0.005+rand.Float64()*(0.01-0.005)))
		updatedUser.Money += profit
		updatedUser.UpdateUserMoney(server.DB, userID)
		context.JSON(200, fmt.Sprintf("You earned %d coin(s)", profit))

	} else {
		//exp money item

		//exp
		updatedUser := gottenUser
		expProfit := 1 + uint64(float64(updatedUser.Experience)*(0.01+rand.Float64()*(0.05-0.01)))
		updatedUser.Experience += expProfit
		updatedUser.UpdateUserExperience(server.DB, userID)

		//money
		moneyProfit := 1 + uint64(float64(updatedUser.Experience)*(0.01*rand.Float64()*(0.04-0.01))+float64(updatedUser.Experience)*(0.005+rand.Float64()*(0.01-0.005)))
		updatedUser.Money += moneyProfit
		updatedUser.UpdateUserMoney(server.DB, userID)

		item := models.Item{}
		generatedItem := item.GenerateItem()

		generatedItem.SaveItem(server.DB)
		foundedItem := models.Item{}
		server.DB.Debug().Model(models.Item{}).Where("name = ? AND rarity = ? AND effect = ?", generatedItem.Name, generatedItem.Rarity, generatedItem.Effect).First(&foundedItem)

		inventory := models.Inventory{
			UserID:   gottenUser.ID,
			ItemID:   foundedItem.ID,
			Quantity: 1,
		}

		inventory.AddItem(server.DB, gottenUser.ID, foundedItem.ID)
		context.JSON(200, fmt.Sprintf("You found %s %s with effect %d \n You got %d point(s) \n You earned %d coin(s)", item.Rarity, item.Name, item.Effect, expProfit, moneyProfit))
	}
}
