package controllers

import (
	"Deolia/api/auth"
	"Deolia/api/models"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (server *Server) GetShop(context *gin.Context) {
	//body, _ := ioutil.ReadAll(context.Request.Body)
	//requestBody := map[string]string{}
	//err := json.Unmarshal(body, &requestBody)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Cannot unmarshal body ")
	//	return
	//}
	//
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}

	//authHeader := context.Request.Header.Get("Authorization")
	authHeader := context.Request.Header.Get("Authorization")

	fmt.Println(authHeader)
	userID := auth.TokenGetId(authHeader)

	shopmodel := models.Shop{}
	shopContent, err := shopmodel.GetShop(server.DB, userID)
	if err != nil {
		context.JSON(http.StatusConflict, err)
		return
	}
	context.JSON(http.StatusOK, shopContent)
}

func (server *Server) GetShopUserItem(context *gin.Context) {
	//body, _ := ioutil.ReadAll(context.Request.Body)
	//requestBody := map[string]string{}
	//err := json.Unmarshal(body, &requestBody)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Cannot unmarshal body ")
	//	return
	//}
	//
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}

	authHeader := context.Request.Header.Get("Authorization")
	fmt.Println(authHeader)
	userID := auth.TokenGetId(authHeader)

	shopmodel := models.Shop{}
	shopContent, err := shopmodel.GetSellingItem(server.DB, userID)
	if err != nil {
		context.JSON(http.StatusConflict, err)
		return
	}
	context.JSON(http.StatusOK, shopContent)
}

func (server *Server) CreateOrder(context *gin.Context) {
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err := json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}
	//
	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}

	authHeader := context.Request.Header.Get("Authorization")
	fmt.Println(authHeader)
	userID := auth.TokenGetId(authHeader)

	itemID, err := strconv.ParseUint(requestBody["item_id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusConflict, "Invalid request")
		return
	}

	price, err := strconv.ParseUint(requestBody["price"], 10, 64)
	if err != nil {
		context.JSON(http.StatusConflict, "Invalid request")
		return
	}

	shop := models.Shop{
		UserID:   userID,
		ItemID:   itemID,
		Quantity: 1,
		Price:    price,
	}
	shopItemID, err := shop.CreateOrder(server.DB, userID, itemID, price)
	if err != nil {
		context.JSON(http.StatusConflict, err)
		return
	}
	context.JSON(http.StatusOK, fmt.Sprintf("Sell offer created ID: %d", shopItemID))

}

func (server *Server) BuyItem(context *gin.Context) {
	body, _ := ioutil.ReadAll(context.Request.Body)
	requestBody := map[string]string{}
	err := json.Unmarshal(body, &requestBody)
	if err != nil {
		context.JSON(http.StatusConflict, "Cannot unmarshal body ")
		return
	}

	//userID, err := strconv.ParseUint(requestBody["id"], 10, 64)
	//if err != nil {
	//	context.JSON(http.StatusConflict, "Invalid request")
	//	return
	//}

	authHeader := context.Request.Header.Get("Authorization")
	fmt.Println(authHeader)
	userID := auth.TokenGetId(authHeader)

	itemID, err := strconv.ParseUint(requestBody["item_id"], 10, 64)
	if err != nil {
		context.JSON(http.StatusConflict, "Invalid request")
		return
	}

	item := models.Shop{}

	boughtItem, err := item.BuyItem(server.DB, userID, itemID)
	if err != nil {
		fmt.Println(err)
		context.JSON(http.StatusConflict, err.Error())
		return
	}
	context.JSON(http.StatusOK, fmt.Sprintf("You bought item: %d", boughtItem))

}
