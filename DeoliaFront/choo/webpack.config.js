const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const WebpackObfuscator = require('webpack-obfuscator');

module.exports = {
    entry: {
        app: './src/app.js', // Your main app entry
        common: './src/common.js', // Your common JavaScript entry
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js', // Use [name] placeholder for dynamic filenames
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html',
        }),
        // new WebpackObfuscator({ // Add the obfuscator plugin
        //     rotateStringArray: true,
        //     stringArray: true,
        //     stringArrayEncoding: ['base64'],
        // }),
    ],
    devServer: {
        static: {
            directory: path.resolve(__dirname, 'dist'),
        },
        historyApiFallback: true,
        hot: true,
    },
};
