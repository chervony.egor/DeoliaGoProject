const html = require('choo/html');
const $ = require('jquery');



module.exports = function (state, emit) {
    function register() {
        data = {
            "login": $("#login").val(),
            "email": $("#email").val(),
            "password": $("#pass").val()
        };


        if (data.login === "") {
            document.querySelector("#alert").innerHTML = `<div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                Login empty
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
            return;
        }

        if (data.email === "") {
            document.querySelector("#alert").innerHTML = `<div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                Email empty
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
            return;
        }

        if (data.password === "") {
            document.querySelector("#alert").innerHTML = `<div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                Password empty
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
            return;
        }


        $.ajax({
            url: "http://localhost:8080/api/createUser",
            method: "POST",
            async: true,
            contentType: 'application/json', // Set the correct content type
            data: JSON.stringify(data),
            success: function (response) {
                $.ajax({
                    url: "http://localhost:8080/api/login",
                    method: "POST",
                    async: true,
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (response) {
                        localStorage.setItem("Bearer", response)
                        window.location.href = '/'
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            },
            error: function (err) {
                document.querySelector("#alert").innerHTML = `
                 <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                     ${err.responseJSON}
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                 </div>`
                console.log(err);
            }
        });
    }
    return html`
    <div>
        <div class="mx-auto w-50">
            <div class="text-center">
                <h1>Register</h1>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Login</label>
                <input type="text" class="form-control" id="login" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="email" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="pass">
            </div>
            <div class="text-end">
                <p><a class="link-opacity-100" href="/login">Login</a></p>
            </div>
            <button class="btn btn-primary " onclick="${register}">Register</button>
        </div>
    </div>
  `;
};




