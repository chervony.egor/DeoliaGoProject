const html = require('choo/html');
const $ = require('jquery')

module.exports = function (state, emit) {

    // function getInventory() {
    //     data =  {
    //         "id": localStorage.getItem("id")
    //     };
    //     $.ajax({
    //         url: "http://localhost:8080/api/getInventory",
    //         method: "PUT",
    //         // async: true,
    //         contentType: 'application/json', // Set the correct content type
    //         data: JSON.stringify(data),
    //         success: function (response) {
    //             response = response.reverse()
    //             console.log(response);
    //             var inventory = document.querySelector("#inv")
    //             inventory.innerHTML = "";
    //
    //             for (var element of response){
    //                 console.log(element.id)
    //                 var block = document.createElement("div")
    //                 block.className = "col"
    //                 var card = document.createElement("div")
    //                 card.className = "card mx-auto w-75"
    //                 card.style.width = "18rem"
    //                 var cardbody = document.createElement("div")
    //                 cardbody.className = "card-body"
    //                 cardbody.innerHTML = `
    //                 <h5 class="card-title">${element.name}</h5>
    //                      <p class="card-text text-start">Rarity: ${element.rarity}</p>
    //                      <p class="card-text text-start">Effect: ${element.effect}</p>
    //                      <p class="card-text text-start">Quantity: ${element.quantity}</p>
    //                 `
    //                 var selbutton = document.createElement("button")
    //                 selbutton.className = "btn btn-primary"
    //                 selbutton.setAttribute("data-bs-toggle", "modal");
    //                 selbutton.setAttribute("data-bs-target", "#myModal");
    //                 selbutton.innerText = "Sell"
    //                 // selbutton.onclick = createSellHandler(element.id)
    //
    //                 sellmodalbtn = document.querySelector("#modalbtn")
    //                 sellmodalbtn.onclick = createSellHandler(element.id)
    //
    //                 cardbody.appendChild(selbutton)
    //                 card.appendChild(cardbody)
    //                 block.appendChild(card)
    //                 inventory.appendChild(block)
    //             }
    //         },
    //         error: function (err) {
    //             $("#alert").append(html`
    //              <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
    //                  ${err.responseJSON}
    //                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    //              </div>`);
    //             console.log(err.responseJSON);
    //         }
    //     });
    // }


    function getInventory() {
        // data = {
        //     "id": localStorage.getItem("Bearer")
        // };
        $.ajax({
            url: "http://localhost:8080/api/getInventory",
            method: "PUT",
            contentType: 'application/json',
            // data: JSON.stringify(data),
            success: function (response) {
                response = response.reverse();
                console.log(response);

                var tableBody = document.querySelector("#inventoryTableBody");
                tableBody.innerHTML = "";
                var index = 0;
                for (var  element of response) {
                    console.log(element)
                    var row = document.createElement("tr");

                    var indexCell = document.createElement("th");
                    indexCell.scope = "row";
                    indexCell.textContent = index + 1;
                    row.appendChild(indexCell);
                    index++

                    var nameCell = document.createElement("td");
                    nameCell.textContent = element.Name;
                    row.appendChild(nameCell);

                    var rarityCell = document.createElement("td");
                    rarityCell.textContent = element.Rarity;
                    row.appendChild(rarityCell);

                    var effectCell = document.createElement("td");
                    effectCell.textContent = element.Effect;
                    row.appendChild(effectCell);

                    var quantityCell = document.createElement("td");
                    quantityCell.textContent = element.Quantity;
                    row.appendChild(quantityCell);

                    var sellButtonCell = document.createElement("td");
                    var sellButton = document.createElement("button");
                    sellButton.className = "btn btn-primary";
                    sellButton.setAttribute("data-bs-toggle", "modal");
                    sellButton.setAttribute("data-bs-target", "#myModal");
                    sellButton.textContent = "Sell";
                    // sellButton.onclick = createSellHandler(element.id);
                    sellButtonCell.appendChild(sellButton);
                    row.appendChild(sellButtonCell);

                    tableBody.appendChild(row);

                    sellmodalbtn = document.querySelector("#modalbtn")
                    sellmodalbtn.onclick = createSellHandler(element.ID)
                }
            },
            error: function (err) {
                // Error handling code
            }
        });
    }



    function createSellHandler(itemId) {
        return function() {
            sell(itemId);
            window.location.reload();
        };
    }

    function sell(item) {
        data = {
            // "id": localStorage.getItem("id"),
            "item_id": item.toString(),
            "price": document.querySelector("#price").value
        }
        console.log(JSON.stringify(data))

        $.ajax({
            url: "http://localhost:8080/api/sell",
            method: "POST",
            async: true,
            contentType: 'application/json', // Set the correct content type
            data: JSON.stringify(data),
            success: function (response) {
                console.log(response);
                for (var element of response){
                    console.log(element.effect)
                }
            },
            error: function (err) {
                document.querySelector("#alert").innerHTML = `
                 <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                     ${err.responseJSON}
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                 </div>`
                console.log(err.responseJSON);
            }
        });

    }


    window.onload = function () {
            const inventory = document.querySelector("#inv")
           getInventory()
       }

    if (localStorage.getItem("Bearer")) {
        $.ajaxSetup({
            headers: {
                'Authorization': localStorage.getItem("Bearer")
            }
        })}

    // document.addEventListener("DOMContentLoaded", function (){
    //     const inventory = document.querySelector("#inv");
    //     if (inventory) {
    //         inventory.innerHTML = "qwe"; // Змінити текст на бажаний
    //     }
    //     getInventory();
    // });





    return html`
    <div class="mx-auto">
        <div class="text-center">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Sell item</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                <label class="form-label">Price</label>
                                <input type="text" class="form-control" placeholder="Enter item price" id="price" aria-describedby="emailHelp">
                            </div>
                        </div>
                        <div class="modal-footer" >
                            <button type="button" id="modalbtn" class="btn btn-primary" data-bs-dismiss="modal">Sell</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>  
            </div>

            <h2 class="mb-3">Inventory</h2>
            <hr>
<!--            <div class=" mt-3 mx-auto row row-cols-1 row-cols-md-3 g-4" id="inv"></div>-->
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Rarity</th>
                    <th scope="col">Effect</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody id="inventoryTableBody">
                <!-- Table rows will be added here -->
                </tbody>
            </table>

            <!--            <button class="btn btn-primary" onclick="${getInventory}">Get</button>-->
        </div>
        
    </div>
  `;
};
