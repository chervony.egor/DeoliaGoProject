const html = require('choo/html');
const $ = require('jquery')

module.exports = function (state, emit) {


    function login() {
        data = {
            "login": document.querySelector("#login").value,
            "password": document.querySelector("#pass").value
        }
        $.ajax({
            url: "http://localhost:8080/api/login",
            method: "POST",
            async: true,
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (response) {
                localStorage.setItem("Bearer", response)
                window.location.href = '/'
            },
            error: function (err) {
                console.log(err)
            }
        })
    }


    return html`
    <div>
        <div class="mx-auto w-50">
            <div class="text-center">
                <h1 clas="text-center">Login</h1>
            </div>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Login</label>
                <input type="text" class="form-control" id="login" aria-describedby="emailHelp">
<!--                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="pass">
            </div>
            <div class="text-end">
                <p><a class="link-opacity-100" href="/register">Register</a></p>
            </div>
            <div class="text-center">
                <button class="btn btn-primary" onclick="${login}">Login</button>
            </div>
        </div>
    </div>
  `;
};

