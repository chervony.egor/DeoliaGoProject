const html = require('choo/html');
const $ = require('jquery')

module.exports = function (state, emit) {

    // function getShop() {
    //     // data =  {
    //     //     "id": localStorage.getItem("id")
    //     // };
    //     $.ajax({
    //         url: "http://localhost:8080/api/getShop",
    //         method: "GET",
    //         async: true,
    //         contentType: 'application/json', // Set the correct content type
    //         // data: JSON.stringify(data),
    //         success: function (response) {
    //             response = response.reverse()
    //             console.log(response);
    //             var shop = document.querySelector("#shop")
    //             shop.innerHTML = "";
    //
    //             for (var element of response){
    //                 console.log(element)
    //                 var block = document.createElement("div")
    //                 block.className = "col"
    //                 var card = document.createElement("div")
    //                 card.className = "card mx-auto w-75"
    //                 card.style.width = "18rem"
    //                 var cardbody = document.createElement("div")
    //                 cardbody.className = "card-body"
    //                 cardbody.innerHTML = `
    //                 <h5 class="card-title">${element.ItemName}</h5>
    //                      <p class="card-text text-start">Rarity: ${element.ItemRarity}</p>
    //                      <p class="card-text text-start">Effect: ${element.ItemEffect}</p>
    //                      <p class="card-text text-start">Quantity: ${element.Quantity}</p>
    //                      <p class="card-text text-start">Price: ${element.Price}</p>
    //                      <p class="card-text text-start">Seller: ${element.UserName}</p>
    //                 `
    //                 var selbutton = document.createElement("button")
    //                 selbutton.className = "btn btn-success"
    //                 selbutton.innerText = "Buy"
    //                 selbutton.onclick = createSellHandler(element.id)
    //
    //                 cardbody.appendChild(selbutton)
    //                 card.appendChild(cardbody)
    //                 block.appendChild(card)
    //                 shop.appendChild(block)
    //             }
    //         },
    //         error: function (err) {
    //             $("#alert").append(html`
    //              <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
    //                  ${err.responseJSON}
    //                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    //              </div>`);
    //             console.log(err.responseJSON);
    //         }
    //     });
    // }



    function getShop() {
        $.ajax({
            url: "http://localhost:8080/api/getShop",
            method: "GET",
            async: true,
            contentType: 'application/json',
            success: function (response) {
                response = response.reverse();
                console.log(response);
                var tableBody = document.querySelector("#shopTableBody");
                tableBody.innerHTML = "";
                var index = 0;

                // for (var element of response){
                //     if (element.UserName == "eee"){
                //         response.splice(response.indexOf(element),1)
                //     }
                // }

                for (var element of response) {
                        var row = document.createElement("tr");

                        var indexCell = document.createElement("th");
                        indexCell.scope = "row";
                        indexCell.textContent = index + 1;
                        row.appendChild(indexCell);
                        index++;

                        var itemNameCell = document.createElement("td");
                        itemNameCell.textContent = element.ItemName;
                        row.appendChild(itemNameCell);

                        var rarityCell = document.createElement("td");
                        rarityCell.textContent = element.ItemRarity;
                        row.appendChild(rarityCell);

                        var effectCell = document.createElement("td");
                        effectCell.textContent = element.ItemEffect;
                        row.appendChild(effectCell);

                        var quantityCell = document.createElement("td");
                        quantityCell.textContent = element.Quantity;
                        row.appendChild(quantityCell);

                        var sellerCell = document.createElement("td");
                        sellerCell.textContent = element.UserName;
                        row.appendChild(sellerCell);

                        var priceCell = document.createElement("td");
                        priceCell.innerHTML = element.Price + "  <i class=\"bi bi-coin\"></i>";
                        row.appendChild(priceCell);

                        var buyButtonCell = document.createElement("td");
                        var buyButton = document.createElement("button");
                        buyButton.setAttribute("data-bs-toggle", "modal");
                        buyButton.setAttribute("data-bs-target", "#myModal");
                        buyButton.className = "btn btn-success";
                        buyButton.textContent = "Buy";
                        buyButton.onclick = modalBodyChangeHandler(element)
                        buyButtonCell.appendChild(buyButton);
                        row.appendChild(buyButtonCell);

                        tableBody.appendChild(row);
                }
            },
            error: function (err) {
                // Error handling code
            }
        });
    }


    function modalBodyChangeHandler(element){
        return function () {
            modalbody = document.querySelector("#modal-body")
            modalbody.innerHTML = `<h3>You want to buy ${element.ItemRarity} ${element.ItemName} with effect ${element.ItemEffect}</h3>`
            modalBuyButton = document.querySelector("#modalbtn")
            modalBuyButton.onclick = buyHandler(element.ItemID)
        }
    }

    function buyHandler(item) {
        return function() {
            buy(item);
            window.location.reload();
        };
    }

    function buy(item) {
        data = {
            "item_id": item.toString(),
        }
        console.log(JSON.stringify(data))

        $.ajax({
            url: "http://localhost:8080/api/buy",
            method: "POST",
            async: true,
            contentType: 'application/json', // Set the correct content type
            data: JSON.stringify(data),
            success: function (response) {
                console.log(response);
            },
            error: function (err) {
                document.querySelector("#alert").innerHTML = `
                 <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                     ${err.responseJSON}
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                 </div>`
                console.log(err.responseJSON);
            }
        });

    }

    if (localStorage.getItem("Bearer")) {
        $.ajaxSetup({
            headers: {
                'Authorization': localStorage.getItem("Bearer")
            }
        })}

    document.body.onload = function () {
        getShop()
    }

    return html`
    <div class="mx-auto">
        <div class="text-center">


            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buy item</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="modal-body">
                        </div>
                        <div class="modal-footer" >
                            <button type="button" id="modalbtn" class="btn btn-primary" data-bs-dismiss="modal">Buy</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            <h2 class="mb-3">Shop</h2>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="shoptab">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="useritemtab">My Items</a>
                </li>
            </ul>
            <hr>
<!--            <div class=" mt-3 mx-auto row row-cols-1 row-cols-md-3 g-4" id="shop"></div>-->
            
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Item Name</th>
                    <th scope="col">Rarity</th>
                    <th scope="col">Effect</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Seller</th>
                    <th scope="col">Price</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody id="shopTableBody">
                <!-- Table rows will be added here -->
                </tbody>
            </table>
            
        </div>
        
    </div>
  `;
};
