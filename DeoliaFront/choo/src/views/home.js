const html = require('choo/html');
const $ = require('jquery')
module.exports = function (state, emit) {
    let progressWidth = 0;
    function step() {
        const button = document.getElementById("step");
        const progressBar = document.querySelector(".progress-bar");

        if (button && progressBar) {
            button.classList.add("disabled"); // Add the "disabled" class immediately

            const interval = setInterval(function () {
                if (progressWidth < 100) {
                    progressWidth += 20;
                    progressBar.style.width = `${progressWidth}%`;
                } else {
                    clearInterval(interval);
                    progressWidth = 0; // Reset the progress width to 0%
                    progressBar.style.width = `${progressWidth}%`;
                }
            }, 1000); // Every 1000 milliseconds = 1 second

            setTimeout(function () {
                button.classList.remove("disabled");

                $.ajax({
                    url: "http://localhost:8080/api/step",
                    method: "POST",
                    async: true,
                    contentType: 'application/json', // Set the correct content type
                    success: function (response) {
                        console.log(response);
                        document.querySelector("#alert").innerHTML = `<div class="alert alert-success alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                        ${response}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                        userinfo = document.querySelector("#userinfo");
                        $.ajax({
                            url: "http://localhost:8080/api/getUser",
                            method: "GET",
                            async: true,
                            contentType: "application/json",
                            success: function (response) {
                                console.log(response)
                                userinfo.innerHTML = `${response.money} <i class="bi bi-coin"></i>    ${response.experience} <i class="bi bi-stars"></i> <span class="badge text-bg-primary">${response.login}</span> `
                            },
                            error: function (err) {
                                console.log(err)
                            }
                        })
                    },
                    error: function (err) {
                        $("#alert").append(html`
                 <div class="alert alert-warning alert-dismissible fade show w-50 mx-auto mt-2" role="alert">
                     ${err.responseJSON}
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                 </div>`);
                        console.log(err.responseJSON);
                    }
                });


                // Remove the "disabled" class after 5 seconds
            }, 6500);



        }

    }

    function RedirectToLoginHandler() {
        return function () {
            window.location.href="/login"
        }
    }

    function RedirectToRegistrationHandler(){
        return function () {
            window.location.href="/register"
        }
    }

    if (localStorage.getItem("Bearer")) {
        $.ajaxSetup({
            headers: {
                'Authorization': localStorage.getItem("Bearer")
            }
        })
        return html`
            <div class="text-center">
                <h3>Travel</h3>
                <br>
                <button id="step" class="btn btn-outline-primary w-25" onclick="${step}" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="bottom">
                    Take a step
                    <br>
                    <div class="progress mx-auto w-75 mt-2" role="progressbar" aria-label="Basic example"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar" style="width: %"></div>
                    </div>
                </button>
                <div id="stepInfo"></div>

            </div>
        `;
    }
    else {
        return html `
            <div class="text-center">


                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Aletr</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <h3>Please login or register to play</h3>
                                </div>
                            </div>
                            <div class="modal-footer" >
                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="${RedirectToLoginHandler()}">Login</button>
                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="${RedirectToRegistrationHandler()}">Register</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <h3>Travel</h3>
                <br>
                <button id="step" class="btn btn-outline-primary w-25" data-bs-toggle="modal" data-bs-target="#myModal">
                    Take a step
                    <br>
                    <div class="progress mx-auto w-75 mt-2" role="progressbar" aria-label="Basic example"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar" style="width: %"></div>
                    </div>
                </button>
            </div>
        `
    }
};


