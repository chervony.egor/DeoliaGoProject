const choo = require('choo');
const html = require('choo/html');

const app = choo();
/*app.use(cors());*/

app.use((state, emitter) => {
    emitter.on('navigate', () => {
        window.location.reload()
        // switch (state.route) {
        //     case "inventory" :
        //         window.location.reload()
        //         return
        //     case "shop" :
        //         window.location.reload()
        //         return
        // }
        // console.log(`Navigated to ${state.route}`)
    })
})

const homeView = require('./views/home');
const aboutView = require('./views/about');
const contactView = require('./views/contact');

const loginView = require('./views/login');
const registerView = require('./views/register');
const inventoryView = require('./views/inventory');
const shopView = require('./views/shop');


app.route('/', homeView);
app.route('/about', aboutView);
app.route('/contact', contactView);

app.route('/login', loginView);
app.route('/register', registerView);
app.route('/inventory', inventoryView);
app.route('/shop', shopView);

app.mount('#app');
