const $ = require('jquery')
document.addEventListener("DOMContentLoaded", function() {
    const navUl = document.querySelector("#navBtn");
    if (localStorage.getItem("Bearer")) {
        const userinfo = document.createElement("li")
        userinfo.id = "userinfo";
        $.ajax({
            url: "http://localhost:8080/api/getUser",
            method: "GET",
            async: true,
            contentType: "application/json",
            success: function (response) {
                console.log(response)
                userinfo.innerHTML = `${response.money} <i class="bi bi-coin"></i>    ${response.experience} <i class="bi bi-stars"></i> <span class="badge text-bg-primary">${response.login}</span> `
            },
            error: function (err) {
                console.log(err)
            }
        })

        const logoutLi = document.createElement("li");
        logoutLi.className = "nav-item";
        const logoutLink = document.createElement("a");
        logoutLink.className = "nav-link";
        logoutLink.onclick = logout;
        logoutLink.innerHTML = `<i class="bi bi-box-arrow-left"></i>`;
        logoutLink.href = "/";
        logoutLi.appendChild(logoutLink);
        navUl.appendChild(userinfo);
        navUl.appendChild(logoutLi);
    } else {
        const loginLi = document.createElement("li");
        loginLi.className = "nav-item";
        const loginLink = document.createElement("a");
        loginLink.className = "nav-link";
        loginLink.innerHTML = `<i class=\"bi bi-box-arrow-in-right\"></i>`;
        loginLink.href = "/login";
        loginLi.appendChild(loginLink);
        navUl.appendChild(loginLi);
    }
});

function logout() {
    if (localStorage.getItem("Bearer")){
        localStorage.removeItem("Bearer");
        window.location.reload();
    }
}

if (localStorage.getItem("Bearer")) {
    $.ajaxSetup({
        headers: {
            'Authorization': localStorage.getItem("Bearer")
        }
    })}


